# Raid and HBA Card Sources

## Card Flashing
* [Introduction \- Fohdeesha Docs](https://fohdeesha.com/docs/perc.html "Introduction - Fohdeesha Docs")
* [Updated\: SAS HBA crossflashing or flashing to IT mode\, Dell Perc H200 and H310 – techmattr](https://techmattr.wordpress.com/2016/04/11/updated-sas-hba-crossflashing-or-flashing-to-it-mode-dell-perc-h200-and-h310/ "Updated: SAS HBA crossflashing or flashing to IT mode, Dell Perc H200 and H310 – techmattr")

## HBA Recommendations
* [Top Picks for FreeNAS HBAs \(Host Bus Adapters\)](https://www.servethehome.com/buyers-guides/top-hardware-components-for-truenas-freenas-nas-servers/top-picks-truenas-freenas-hbas/ "Top Picks for FreeNAS HBAs \(Host Bus Adapters\)")

## H200
* [Flashing a Dell PERC H200 to IT mode with an R710 \| Justin’s tech tips](https://justinschmitt.com/2019/01/11/dell-perc200-it-mode.html "Flashing a Dell PERC H200 to IT mode with an R710 | Justin’s tech tips")

## H730P
* [PowerEdge\-RAID\-Controller\-H730P\-Spec\-Sheet\.pdf](https://i.dell.com/sites/doccontent/shared-content/data-sheets/en/Documents/PowerEdge-RAID-Controller-H730P-Spec-Sheet.pdf "PowerEdge-RAID-Controller-H730P-Spec-Sheet.pdf")
* [MegaRAID 9361\-8i \(LSI SAS3108\) \| ServeTheHome Forums](https://forums.servethehome.com/index.php?threads/megaraid-9361-8i-lsi-sas3108.29775/ "MegaRAID 9361-8i \(LSI SAS3108\) | ServeTheHome Forums")
* [LSI 3108 IT Mode \| TrueNAS Community](https://www.truenas.com/community/threads/lsi-3108-it-mode.46407/ "LSI 3108 IT Mode | TrueNAS Community")
* [mrsas driver instead of mfi driver \| Page 2 \| TrueNAS Community](https://www.truenas.com/community/threads/mrsas-driver-instead-of-mfi-driver.34632/page-2?__hstc=123088916.0ec83f634e15ccc21fc142cd4ed6c9d6.1710973799207.1710973799207.1710973799207.1&__hssc=123088916.7.1710973799207&__hsfp=2717307630#post-242013 "mrsas driver instead of mfi driver | Page 2 | TrueNAS Community")
* [LSI 3108 Controller JBOD mode \= IT mode\? \| TrueNAS Community](https://www.truenas.com/community/threads/lsi-3108-controller-jbod-mode-it-mode.91842/ "LSI 3108 Controller JBOD mode = IT mode? | TrueNAS Community")
* [How To Convert RAID mode to HBA Mode on Dell PERC \| Dell US](https://www.dell.com/support/contents/en-us/videos/videoplayer/how-to-convert-raid-mode-to-hba-mode-on-dell-perc/6079781997001 "How To Convert RAID mode to HBA Mode on Dell PERC | Dell US")
* [Switching the Dell Perc H370p to HBA Mode \| by Carl Liebich \| Medium](https://carll.medium.com/switching-the-dell-perc-h370p-to-hba-mode-f38f286102a2 "Switching the Dell Perc H370p to HBA Mode | by Carl Liebich | Medium")

[Back](../readme.md)
